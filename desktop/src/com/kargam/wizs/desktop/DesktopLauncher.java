package com.kargam.wizs.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.kargam.wizs.WizsGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Wizs";
            config.width = 1920 / 2;
            config.height = 1080 / 2;
        config.resizable = false;
        new LwjglApplication(new WizsGame(), config);
	}
}