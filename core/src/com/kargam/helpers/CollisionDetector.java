package com.kargam.helpers;

import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.Enemy;
import com.kargam.gameobjects.Projectile;
import com.kargam.gameobjects.StaticObject;
import com.kargam.gameworld.GameWorld;

/**
 * Created by Karl on 02/11/2014.
 */
public class CollisionDetector {

    private static GameWorld world;

    public static void init(GameWorld gameWorld) {
        world = gameWorld;
    }

    /**
     * checks if AbstractWiz has collided with static objects or other wizs.
     *
     * @param wizGroundBox
     * @return
     */
    public static boolean hasWizCollided(AbstractWiz abstractWiz, Rectangle wizGroundBox) {
        if (!abstractWiz.equals(world.getPlayer()) &&
                Intersector.overlaps(wizGroundBox, world.getPlayer().getBody().getGroundBox())) {
            return true;
        }
        for (int i = 0; i < world.getEnemies().size; i++) {
            AbstractWiz abstractWizComp = world.getEnemies().get(i);
            if (!abstractWiz.equals(abstractWizComp) &&
                    Intersector.overlaps(wizGroundBox, abstractWizComp.getBody().getGroundBox())) {
                return true;
            }
        }
        for (int i = 0; i < world.getArena().getStaticObjects().size; i++) {
            if (Intersector.overlaps(wizGroundBox,
                    world.getArena().getStaticObjects().get(i).getHitBox())) {
                return true;
            }
        }
        return false;
    }

    public static boolean hasProjectileCollided(Projectile p){
        if(p.isFriendly()){
                for(Enemy e: world.getEnemies()){
                    if(Intersector.overlaps(p.getBoundingCircle(), e.getBody().getHitBox())){
                        p.hitWiz(e);
                        return true;
                    }
            }
        }else if(!p.isFriendly()){
            if(Intersector.overlaps(p.getBoundingCircle(), world.getPlayer().getBody().getHitBox())){
                p.hitWiz(world.getPlayer());
                return true;
            }
        }

        for(StaticObject sO: world.getArena().getStaticObjects()){
            if(Intersector.overlaps(p.getBoundingCircle(), (sO.getHitBox()))){
                p.hitWall();
                return true;
            }
        }
        return false;
    }
}