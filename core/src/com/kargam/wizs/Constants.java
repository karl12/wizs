package com.kargam.wizs;

public class Constants {

    public static final int BASIC_PROJECTILE_VELOCITY = 5;

    //Base action settings
    public static final float BASE_TELEPORT_COOLDOWN = 10;

    public static final float BASE_FIREBALL_COOLDOWN = 30;
    public static final int BASE_FIREBALL_DAMAGE = 5;


    public static final float BASE_PHYSICAL_HIT_COOLDOWN = 0.3f;
    public static final int BASE_PHYSICAL_HIT_DAMAGE = 1;
    public static final int PROJECTILE_RADIUS = 10;
    public static final boolean DEBUGGING_MODE = true;
    public static float BASE_RATE_OF_FILE = 0.3f;
    public static int ARENA_WIDTH = 3200;
    public static int ARENA_HEIGHT = 480;

    public static int WIZ_SPRITE_RENDER_SIZE;
    public static int WIZ_WIDTH;
    public static int WIZ_HEIGHT;

    public static int targetLocationSize = 1;
    public static int BASE_WIZ_MAX_VELOCITY = 80;
}
