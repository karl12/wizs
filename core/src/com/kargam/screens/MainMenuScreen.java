package com.kargam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kargam.helpers.AssetLoader;
import com.kargam.wizs.WizsGame;

public class MainMenuScreen implements Screen {

    private WizsGame game;


    private Stage stage;
    private Table table;
    private TextButton startButton;
    private TextButton exitButton;
    private TextButton resetButton;
    private int buttonWidth;
    private int buttonHeight;

    /**
     * Main menu
     *
     * @param game
     */
    public MainMenuScreen(WizsGame game) {
        this.game = game;
        stage = new Stage();

        table = new Table();
        stage.addActor(table);

        createStartButton();
        createExitButton();
        createResetButton();

        //Fill table with buttons
        buttonWidth = Gdx.graphics.getWidth() / 3;
        buttonHeight = Gdx.graphics.getHeight() / 5;

        table.add(startButton).width(buttonWidth).height(buttonHeight);
        table.row();
        table.add(exitButton).width(buttonWidth).height(buttonHeight);
        table.setFillParent(true);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);

    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    private void createStartButton() {
        startButton = new TextButton("Start", AssetLoader.tbStyle);
        startButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(game.getGameScreen());
                startButton.setText("Resume");

                table.reset();

                table.add(startButton).width(buttonWidth).height(buttonHeight);
                table.row();
                table.add(resetButton).width(buttonWidth).height(buttonHeight);
                table.row();
                table.add(exitButton).width(buttonWidth).height(buttonHeight);
            }
        });
    }

    private void createResetButton(){
        resetButton = new TextButton("Restart", AssetLoader.tbStyle);
        resetButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.getGameScreen().getGameWorld().restart();
                game.setScreen(game.getGameScreen());
            }
        });

    }

    private void createExitButton() {
        exitButton = new TextButton("Exit Game", AssetLoader.tbStyle);
        exitButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.log("Exit", "Pressed");
                Gdx.app.exit();
            }
        });
    }
}
