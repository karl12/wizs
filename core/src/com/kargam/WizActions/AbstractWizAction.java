package com.kargam.WizActions;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.SpellAnimationSet;

/**
 * Abstract class for spells.
 *
 * @author Karl
 */
public abstract class AbstractWizAction {

    protected String name;

    protected AbstractWiz owner;
    protected GameWorld world;

    protected boolean isActive;

    protected int damage;

    private float cooldownTime;
    private float currentCooldown;

    protected TextureRegion actionIcon;

    protected SpellAnimationSet animationSet;


    public AbstractWizAction(AbstractWiz owner, GameWorld world) {
        this.owner = owner;
        this.world = world;
        setCooldownTime(0);

        setBaseCooldown();
        setDamage();
    }

    public void reduceCooldown(float delta) {
        if (currentCooldown > 0) {
            currentCooldown -= delta;
        } else {
            currentCooldown = 0;
        }
    }

    protected abstract void setBaseCooldown();

    protected abstract void setDamage();

    public abstract void use(float posX, float posY, AbstractWiz target);

    public void toggleActive() {
        if (!isActive && currentCooldown == 0) {
            isActive = true;
        } else {
            isActive = false;
        }
    }

    /**
     * Called on death of wiz
     */
    public void reset() {
        isActive = false;
        currentCooldown = 0;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setIsActive(boolean b) {
        isActive = b;
    }

    public boolean isActive() {
        return isActive;
    }

    public float getCurrentCooldown() {
        return currentCooldown;
    }

    public void setCurrentCooldown(float currentCooldown) {
        this.currentCooldown = currentCooldown;
    }

    public float getCooldownTime() {
        return cooldownTime;
    }

    public void setCooldownTime(float cooldownTime) {
        this.cooldownTime = cooldownTime;
    }

    public TextureRegion getActionIcon() {
        return actionIcon;
    }

}