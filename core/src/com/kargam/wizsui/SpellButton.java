package com.kargam.wizsui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kargam.WizActions.AbstractWizAction;
import com.kargam.helpers.AssetLoader;

public class SpellButton extends Actor {

    private Circle drawingArea;

    private AbstractWizAction abstractWizAction;
    private ShapeRenderer renderer;
    private SpriteBatch batcher;
    private Vector2 position;

    public SpellButton(AbstractWizAction abstractWizAction, ShapeRenderer renderer, SpriteBatch batcher) {

        this.abstractWizAction = abstractWizAction;

        this.renderer = renderer;
        this.batcher = batcher;

        this.addListener(new spellActiveToggle());

        debug();
    }

    /**
     * Must be called after adding to the table to get the correct coordinates.
     */
    public void init() {

        Vector2 stagePosition = this.localToStageCoordinates(new Vector2(this.getOriginX(),
                this.getOriginY()));
        position = this.getStage().stageToScreenCoordinates(stagePosition);

        double radius = (getHeight() / 2);

        drawingArea = new Circle();
        drawingArea.radius = (float) radius;
        drawingArea.x = position.x + drawingArea.radius;
        drawingArea.y =  (position.y + (drawingArea.radius*3));


    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.end();

        init();

        renderer.begin(ShapeType.Line);
        renderer.circle(drawingArea.x, drawingArea.y, drawingArea.radius);
        renderer.end();

        TextureRegion tr = abstractWizAction.getActionIcon();
        Sprite sp = new Sprite(tr);
        sp.setPosition(drawingArea.x - drawingArea.radius, drawingArea.y
                - drawingArea.radius);
        sp.setSize(drawingArea.radius * 2, drawingArea.radius * 2);

        batcher.enableBlending();

        batcher.begin();
        sp.draw(batcher, 0.9f);
        batcher.end();


        if (abstractWizAction.getCurrentCooldown() != 0) {

            Gdx.gl.glEnable(GL20.GL_BLEND);
            Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

            renderer.begin(ShapeType.Filled);
            renderer.setColor(0f, 0.f, 0.f, 0.95f);
            int teleportDeg = (int) ((1 - (abstractWizAction.getCurrentCooldown() / abstractWizAction
                    .getCooldownTime())) * 360);
            renderer.arc(drawingArea.x, drawingArea.y, drawingArea.radius, 270,
                    teleportDeg);
            renderer.end();


            Gdx.gl.glDisable(GL20.GL_BLEND);
        }


        batch.begin();


    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        if (touchable && getTouchable() != Touchable.enabled) {
            return null;
        } else {
            Rectangle boundingRect = new Rectangle();
            boundingRect.setSize(drawingArea.radius * 2);

            Vector2 position = new Vector2(drawingArea.x, drawingArea.y);
            this.stageToLocalCoordinates(position);

            boundingRect.setCenter(position);

            if (boundingRect.contains(x, y)) {
                return this;
            } else {
                return null;
            }
        }
    }

    private class spellActiveToggle extends ClickListener {

        @Override
        public void clicked(InputEvent event, float x, float y) {
            abstractWizAction.toggleActive();
        }
    }
}
