package com.kargam.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Karl on 12/11/2014.
 */
public abstract class AbstractWizObject extends Actor implements Comparable {

    public abstract void render(float runTime, float delta, ShapeRenderer renderer, SpriteBatch batch);

    public abstract int getScreenY();

    @Override
    public int compareTo(Object o) {
        AbstractWizObject AWO = (AbstractWizObject) o;
        return (AWO.getScreenY() - this.getScreenY());
    }

}
