package com.kargam.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.kargam.wizs.Constants;

public class AssetLoader {

    //SpriteSheets
    public static Texture ssBland;
    public static Texture ssSekeleton;

    public static Texture spell;
    public static TextureRegion trSpell;

    public static Texture groundTexture;
    public static TextureRegion trGround;

    public static TextureAtlas blueButtons;
    public static TextureAtlas spellButtons;

    public static TextureRegion teleportTxr;
    public static TextureRegion bombTxr;
    public static TextureRegion fireballTxr;
    public static TextureRegion rayTxr;

    public static Texture fireballSpriteTxr;

    public static BitmapFont font;
    public static Skin blueSkin;
    public static TextButtonStyle tbStyle;
    public static TextButtonStyle pauseStyle;
    public static Table table;

    public static TiledMap tiledMap;

    public static void load() {
        tiledMap = new TmxMapLoader().load("maps/map1.tmx");

        ssBland = new Texture(Gdx.files.internal("charactersprites/bland.png"));
        ssSekeleton = new Texture(Gdx.files.internal("charactersprites/skeleton.png"));

        spell = new Texture(Gdx.files.internal("spell.png"));
        trSpell = new TextureRegion(spell, 20, 20);

        blueButtons = new TextureAtlas(Gdx.files.internal("buttons/bluebuttons.pack"));
        spellButtons = new TextureAtlas(Gdx.files.internal("buttons/spells.atlas"));

        groundTexture = new Texture(Gdx.files.internal("groundtexture/stonefloor.jpg"));
        trGround = new TextureRegion(groundTexture, 256, 256);

        //spell images
        teleportTxr = spellButtons.findRegion("caged-ball");
        fireballTxr = spellButtons.findRegion("rocket");

        //spell sprites
        fireballSpriteTxr = new Texture(Gdx.files.internal("spellsprites/fireball_0.png"));


        //UI buttons
        font = new BitmapFont();
        font.setScale(Constants.ARENA_HEIGHT / 200);

        blueSkin = new Skin(AssetLoader.blueButtons);

        tbStyle = new TextButtonStyle();
        tbStyle.font = font;
        tbStyle.up = blueSkin.getDrawable("blue_button00");
        tbStyle.down = blueSkin.getDrawable("blue_button02");
        tbStyle.checked = blueSkin.getDrawable("blue_button04");

        pauseStyle = new TextButtonStyle();
        pauseStyle.font = font;

        pauseStyle.up = blueSkin.getDrawable("blue_boxCross");

    }

    public static void dispose() {
        blueButtons.dispose();
        ssBland.dispose();
        groundTexture.dispose();
        ssSekeleton.dispose();
        spell.dispose();
        tiledMap.dispose();
        fireballSpriteTxr.dispose();
    }
}