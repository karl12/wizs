package com.kargam.wizs;

import com.badlogic.gdx.Game;
import com.kargam.helpers.AssetLoader;
import com.kargam.screens.GameScreen;
import com.kargam.screens.MainMenuScreen;

public class WizsGame extends Game {
    private GameScreen gameScreen;
    private MainMenuScreen mainMenuScreen;

    @Override
    public void create() {
        AssetLoader.load();

        gameScreen = new GameScreen(this);
        mainMenuScreen = new MainMenuScreen(this);

        setScreen(mainMenuScreen);
    }

    @Override
    public void dispose() {
        super.dispose();
        AssetLoader.dispose();
    }

    public GameScreen getGameScreen() {
        return gameScreen;
    }

    public MainMenuScreen getMainMenuScreen() {
        return mainMenuScreen;
    }
}
