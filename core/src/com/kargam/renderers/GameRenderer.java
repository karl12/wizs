package com.kargam.renderers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.utils.Array;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.AbstractWizObject;
import com.kargam.gameobjects.Projectile;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.AssetLoader;
import com.kargam.wizsui.UserInterface;

public class GameRenderer {

    private GameWorld world;

    //game
    private ShapeRenderer renderer;
    private SpriteBatch batcher;

    private OrthogonalTiledMapRenderer mapRenderer;

    //ui renderers
    private ShapeRenderer uiRenderer;
    private SpriteBatch uiBatcher;

    private UserInterface ui;

    private OrthographicCamera gameCam;
    private OrthographicCamera uiCam;

    public GameRenderer(GameWorld world, OrthographicCamera cam, OrthographicCamera uiCam,
                        UserInterface ui) {

        this.ui = ui;
        this.world = world;

        this.gameCam = cam;
        this.uiCam = uiCam;

        mapRenderer = new OrthogonalTiledMapRenderer(AssetLoader.tiledMap);

        batcher = new SpriteBatch();
        batcher.setProjectionMatrix(cam.combined);

        renderer = new ShapeRenderer();
        renderer.setProjectionMatrix(cam.combined);

        uiBatcher = new SpriteBatch();
        uiBatcher.setProjectionMatrix(uiCam.combined);

        uiRenderer = new ShapeRenderer();
        uiRenderer.setProjectionMatrix(uiCam.combined);

        ui.init(uiRenderer, uiBatcher);

        Gdx.gl20.glEnable(GL20.GL_BLEND);
        Gdx.gl20.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

    }

    private void renderWizObjects(float runTime, float delta) {

        Array<AbstractWizObject> renderOrder = new Array<AbstractWizObject>();

        renderOrder.add(world.getPlayer());
        for (Projectile p: world.getPlayer().getProjectiles()){
            renderOrder.add(p);
        }
        for (AbstractWiz w: world.getEnemies()) {
            renderOrder.add(w);
            for(Projectile p: w.getProjectiles())
                renderOrder.add(p);
        }

        renderOrder.sort();

        for (AbstractWizObject wO: renderOrder) {
            wO.render(runTime, delta, renderer, batcher);
        }
    }
    public void render(float runTime, float delta) {

        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        /* Draw Background color
        batcher.begin();
        AssetLoader.groundTexture.setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        batcher.draw(AssetLoader.groundTexture, 0, 0, Constants.ARENA_WIDTH, Constants.ARENA_HEIGHT, 0, 0, 5, 5);
        batcher.end();
        */

        mapRenderer.render();

        renderWizObjects(runTime, delta);

        ui.render(renderer, batcher);

    }

    public void updateCams() {
        renderer.setProjectionMatrix(gameCam.combined);
        batcher.setProjectionMatrix(gameCam.combined);
        mapRenderer.setView(gameCam);
    }
}
