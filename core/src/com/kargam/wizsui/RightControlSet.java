package com.kargam.wizsui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.kargam.WizActions.AbstractWizAction;
import com.kargam.gameobjects.Player;
import com.kargam.helpers.AssetLoader;
import com.kargam.wizs.WizsGame;

public class RightControlSet {

    private Table uiTable;
    private Button pauseButton;
    private SpellButton teleportSpellButton;

    private WizsGame wizsGame;

    private Player player;

    private Stage uiStage;
    private ShapeRenderer renderer;
    private SpriteBatch batcher;

    private Array<SpellButton> spellButtons;

    public RightControlSet(Player player, Stage uiStage, WizsGame wizsGame) {

        this.wizsGame = wizsGame;
        this.player = player;
        this.uiStage = uiStage;

        spellButtons = new Array<SpellButton>();

        createPauseButton(64);

    }

    public void init(ShapeRenderer renderer, SpriteBatch batcher) {
        this.renderer = renderer;
        this.batcher = batcher;

        for(AbstractWizAction awA:player.getWizActions()){
            spellButtons.add(new SpellButton(awA, renderer, batcher));
        }

        uiTable = new Table();

        int pauseBtnSize = Gdx.graphics.getHeight() / 10;
        int spellBtnSize = Gdx.graphics.getHeight() / 5;

        uiTable.align(Align.topRight);

        this.uiStage.addActor(uiTable);
        uiTable.setFillParent(true);
        uiTable.add(pauseButton).width(pauseBtnSize).height(pauseBtnSize);

        //uiTable.add(spellButtons).width(spellBtnSize).height(spellBtnSize);


        for(SpellButton sB: spellButtons){
            uiTable.row();
            uiTable.add(sB).width(spellBtnSize).height(spellBtnSize);
        }

        uiTable.debug();


    }

    private void createPauseButton(int width) {
        pauseButton = new TextButton("", AssetLoader.pauseStyle);
        pauseButton.setScale(width);

        pauseButton.addListener(new ChangeListener() {

            @Override
            public void changed(ChangeEvent event, Actor actor) {
                wizsGame.setScreen(wizsGame.getMainMenuScreen());
            }
        });
    }

    public Button getPauseButton() {
        return pauseButton;
    }

}
