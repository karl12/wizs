package com.kargam.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Karl on 19/11/2014.
 */
public class SpellAnimationSet {

    private Animation left;
    private Animation leftUp;
    private Animation up;
    private Animation rightUp;
    private Animation right;
    private Animation rightDown;
    private Animation down;
    private Animation leftDown;

    public SpellAnimationSet(Texture t) {
        left = generateAnimations(t, 1);
        leftUp = generateAnimations(t, 2);
        up = generateAnimations(t, 3);
        rightUp = generateAnimations(t, 4);
        right = generateAnimations(t, 5);
        rightDown = generateAnimations(t, 6);
        down = generateAnimations(t, 7);
        leftDown = generateAnimations(t, 8);
    }

    private Animation generateAnimations(Texture texture, int row) {
        Array<TextureRegion> array = new Array<TextureRegion>();

        int y = (row - 1) * 64;
        for (int i = 0; i < 8; i++) {
            int x = i * 64;
            TextureRegion tr = new TextureRegion(texture, x, y, 64, 64);
            array.add(tr);
        }
        return new Animation(0.03f, array, Animation.PlayMode.LOOP);
    }

    public Animation getAnimation(float dir){
        Animation returnAnimation = null;
        Gdx.app.log("dir", dir+"");
        if ((dir >= 357.5 && dir < 360) ||
                (dir >= 0 && dir < 22.5)){
            returnAnimation = right;
        }else if(dir >= 22.5 && dir < 67.5) {
            returnAnimation = rightUp;
        } else if (dir >= 67.5 && dir < 112.5) {
            returnAnimation = up;
        } else if (dir >= 112.5 && dir < 137.5) {
            returnAnimation = leftUp;
        } else if (dir >= 137.5 && dir < 202.5){
            returnAnimation = left;
        } else if (dir >= 202.5 && dir < 247.5){
            returnAnimation = leftDown;
        } else if (dir >= 247.5 && dir < 312.5){
            returnAnimation = down;
        } else if (dir >= 312.5 && dir < 357.5){
            returnAnimation = rightDown;
        }

        return returnAnimation;
    }

    public Animation getLeft() {
        return left;
    }

    public Animation getLeftUp() {
        return leftUp;
    }

    public Animation getUp() {
        return up;
    }

    public Animation getRightUp() {
        return rightUp;
    }

    public Animation getRight() {
        return right;
    }

    public Animation getRightDown() {
        return rightDown;
    }

    public Animation getDown() {
        return down;
    }

    public Animation getLeftDown() {
        return leftDown;
    }
}
