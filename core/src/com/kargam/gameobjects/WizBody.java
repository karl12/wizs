package com.kargam.gameobjects;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.kargam.helpers.CollisionDetector;
import com.kargam.wizs.Constants;

/**
 * Created by karl on 09/11/2014.
 */
public class WizBody {

    private Rectangle groundBox;
    private Rectangle hitBox;

    private Vector2 velocity;

    private AbstractWiz abstractWiz;

    //debugging
    private float runtime;

    public WizBody(AbstractWiz abstractWiz, Vector2 position) {
        this.abstractWiz = abstractWiz;
        groundBox = new Rectangle(0, 0, Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT / 2);
        groundBox.setCenter(position);
        Vector2 hitBoxPos = new Vector2(groundBox.getPosition(new Vector2()));

        hitBox = new Rectangle(hitBoxPos.x, hitBoxPos.y, Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT);

        velocity = new Vector2();
    }

    public void update(float delta) {


        //to check at end if abstractWiz has moved or not
        Vector2 currentPosition = new Vector2();
        groundBox.getPosition(currentPosition);

        //Get scaled velocity of movement
        Vector2 scaledVelocity = new Vector2(velocity.cpy().scl(delta));

        //Create new hitbox to give to CollisionDetector
        Rectangle newGroundBox = new Rectangle(groundBox);
        Vector2 newPosition = new Vector2();
        newGroundBox.getPosition(newPosition);

        //Add x velocity and check if X-cord movements collides
        newPosition.add(scaledVelocity.x, 0);
        newGroundBox.setPosition(newPosition);

        if (!CollisionDetector.hasWizCollided(abstractWiz, newGroundBox)){
            groundBox.setPosition(newPosition.x, groundBox.getPosition(new Vector2()).y);
            abstractWiz.moveBy(scaledVelocity.x, 0);
        }

        //reset
        newGroundBox.setPosition(currentPosition);
        newPosition.set(currentPosition);

        //Add y velocity and check
        newPosition.add(0, scaledVelocity.y);
        newGroundBox.setPosition(newPosition);

        if (!CollisionDetector.hasWizCollided(abstractWiz, newGroundBox)){
            groundBox.setPosition(groundBox.getPosition(new Vector2()).x, newPosition.y);

            abstractWiz.moveBy(0, scaledVelocity.y);
        }

        //move hitBox in position relative to groundBox
        hitBox.setPosition(groundBox.x, groundBox.y);

        groundBox.getPosition(newPosition);
        if (currentPosition.equals(newPosition) && abstractWiz.getCurrentAction().equals(WizAction.MOVING)) {
            velocity.setZero();
            abstractWiz.setCurrentAction(WizAction.NOTHING);
        }


        /*Vector2 newPos = hitBox.getPosition(new Vector2());
        newPos.add(velocity.cpy().scl(delta));

        hitBox.setPosition(newPos.cpy());
        newPos.add(0, hitBox.getHeight() / 2);
        groundBox.setPosition(newPos);*/
    }

    public Vector2 getCenter() {
        return groundBox.getPosition(new Vector2()).cpy().add(Constants.WIZ_WIDTH / 2, Constants.WIZ_WIDTH / 4);
    }

    public void setPosition(Vector2 position) {
        groundBox.setCenter(position);
        hitBox.setPosition(groundBox.getX(), groundBox.getY());
    }

    public void resize() {
        hitBox.setSize(Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT);
        groundBox.setSize(Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT / 2);
    }

    public Rectangle getGroundBox() {
        return groundBox;
    }

    public void setGroundBox(Rectangle groundBox) {
        this.groundBox = groundBox;
    }

    public Rectangle getHitBox() {
        return hitBox;
    }

    public void setHitBox(Rectangle hitBox) {
        this.hitBox = hitBox;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }
}
