package com.kargam.WizActions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.Projectile;
import com.kargam.gameobjects.WizAction;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.AssetLoader;
import com.kargam.wizs.Constants;

/**
 * Created by Karl on 14/11/2014.
 */
public class WandAttack extends AbstractWizAction {
    public WandAttack(AbstractWiz owner, GameWorld world) {
        super(owner, world);
        damage = 1;
    }

    @Override
    protected void setBaseCooldown() {
        setCooldownTime(Constants.BASE_RATE_OF_FILE);
    }

    @Override
    protected void setDamage() {
        setDamage(1);
    }

    @Override
    public void use(float posX, float posY, AbstractWiz target) {

        //Controls rate of fire
        if (getCurrentCooldown() <= 0 && target != null) {
            owner.setCurrentAction(WizAction.WEAPON_ATTACK);

            Gdx.app.log("current cool", getCurrentCooldown() + "");

            Vector2 pos = new Vector2();
            owner.getBody().getHitBox().getPosition(pos);

            Vector2 aimPos = new Vector2();
            owner.getEnemyTarget().getBody().getHitBox().getPosition(aimPos);

            aimPos.sub(pos);

            aimPos.limit(owner.getMaxProjectileVelocity());

            Array<TextureRegion> atr = new Array<TextureRegion>();
            atr.add(AssetLoader.trSpell);
            Animation animation = new Animation(0f, atr);

            owner.getProjectiles().add(
                    new Projectile(owner.getBody().getCenter().add(aimPos.cpy().scl(.3f)), aimPos, damage,
                            owner.isFriendly(), owner.getWorld(), animation));

            setCurrentCooldown(getCooldownTime());
            if (owner.getEnemyTarget().getCurrentHealth() <= 0) {
                isActive = false;
                owner.setCurrentAction(WizAction.NOTHING);
            }

        }
    }
}
