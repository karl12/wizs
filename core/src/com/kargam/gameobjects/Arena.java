package com.kargam.gameobjects;

import com.badlogic.gdx.utils.Array;
import com.kargam.wizs.Constants;

/**
 *
 * Game arena within which the wizs are confined.
 *
 * @author Karl
 *
 */

public class Arena {

    private Array<StaticObject> staticObjects;

    public Arena() {
        staticObjects = new Array<StaticObject>();

        //left wall
        staticObjects.add(new StaticObject(2, 0, 1, Constants.ARENA_HEIGHT));

        //top wall
        staticObjects.add(new StaticObject(0, 64, Constants.ARENA_WIDTH, 2));

        //right wall
        staticObjects.add(new StaticObject(Constants.ARENA_WIDTH - 2, 0, 2, Constants.ARENA_HEIGHT));

        //bottom wall
        staticObjects.add(new StaticObject(0, Constants.ARENA_HEIGHT - 2, Constants.ARENA_WIDTH, 2));

    }

    public Array<StaticObject> getStaticObjects(){
        return staticObjects;
    }
}