package com.kargam.gameobjects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;

/**
 * Created by Karl on 02/11/2014.
 */
public class StaticObject extends AbstractWizObject{

    private Rectangle hitBox;

    public StaticObject(int posX, int posY, int width, int height){
        hitBox = new Rectangle(posX, posY, width, height);
    }

    public Rectangle getHitBox(){
        return hitBox;
    }

    @Override
    public int getScreenY() {
        return (int) hitBox.getY();
    }

    @Override
    public void render(float runTime, float delta, ShapeRenderer renderer, SpriteBatch batch) {

    }
}