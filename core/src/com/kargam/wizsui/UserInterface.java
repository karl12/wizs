package com.kargam.wizsui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kargam.gameworld.GameWorld;
import com.kargam.wizs.WizsGame;

public class UserInterface {

    private RightControlSet rightControlSet;

    private Stage uiStage;

    public UserInterface(GameWorld game, Stage uiStage, WizsGame wizsGame) {

        rightControlSet = new RightControlSet(game.getPlayer(), uiStage, wizsGame);
        setRightControlSet(new RightControlSet(game.getPlayer(), uiStage, wizsGame));
        this.uiStage = uiStage;
    }

    public void render(ShapeRenderer renderer, SpriteBatch batcher) {
        uiStage.draw();
    }

    public RightControlSet getRightControlSet() {
        return rightControlSet;
    }

    public void setRightControlSet(RightControlSet rightControlSet) {
        this.rightControlSet = rightControlSet;
    }

    public void init(ShapeRenderer renderer, SpriteBatch batcher) {
        rightControlSet.init(renderer, batcher);
    }
}
