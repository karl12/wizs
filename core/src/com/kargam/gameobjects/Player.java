package com.kargam.gameobjects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.kargam.WizActions.AbstractWizAction;
import com.kargam.WizActions.FireballSpell;
import com.kargam.WizActions.TeleportSpell;
import com.kargam.WizActions.WandAttack;
import com.kargam.gameworld.GameWorld;
import com.kargam.wizs.Constants;

public class Player extends AbstractWiz {

    //location to move to upon reset
    private Vector2 screenCenter;
    private Circle targetLocation;


    public Player(String name, Vector2 position, GameWorld world, Texture texture) {
        super(name, position, world, true, texture);

        screenCenter = position;

        targetLocation = new Circle();

        setBaseAttack(new WandAttack(this, world));


        setMaxHealth(5);
        setCurrentHealth(getMaxHealth());

        getWizActions().add(new TeleportSpell(this, world));
        getWizActions().add(new FireballSpell(this, world));

        this.setBounds(getX(), getY(), Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT);
        this.setSize(Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT);

        setMaxVelocity(Constants.BASE_WIZ_MAX_VELOCITY * 2);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (getCurrentAction().equals(WizAction.MOVING)) {
            moveToTargetLocation();
        } else if (getBaseAttack().isActive()) {
            getBaseAttack().use(0, 0, getEnemyTarget());
        }

        body.update(delta);
    }


    /**
     * Moves player towards target location
     */
    private void moveToTargetLocation() {

        Vector2 playerPos = getBody().getCenter();

        if (getCurrentAction().equals(WizAction.MOVING) && targetLocation.contains(playerPos)) {
            setVelocity(new Vector2(0, 0));
            setCurrentAction(WizAction.NOTHING);
        } else if (getCurrentAction().equals(WizAction.MOVING)) {

            Vector2 loc = new Vector2(targetLocation.x, targetLocation.y);
            setVelocity(loc.sub(playerPos));
        }
    }

    @Override
    public void reduceHealth(int damage) {
        setCurrentHealth(getCurrentHealth() - damage);
        if (getCurrentHealth() <= 0) {
        }

        Gdx.app.log("player health", getCurrentHealth() + "");
    }

    public void reset() {
        setCurrentHealth(getMaxHealth());
        getBody().setPosition(screenCenter);
        getBody().getVelocity().setZero();
        setEnemyTarget(null);

        for (int i = 0; i < getWizActions().size; i++) {
            getWizActions().get(i).reset();
        }

        setCurrentAction(WizAction.NOTHING);
        getWizRenderer().updateBlinker(100);
    }

    public void setTargetLocation(float screenX, float screenY) {

        for (AbstractWizAction wA : getWizActions()) {
            wA.setIsActive(false);
        }
        getBaseAttack().setIsActive(false);

        if (isMoveable()) {

            targetLocation = new Circle(screenX, screenY, Constants.targetLocationSize);
            setCurrentAction(WizAction.MOVING);
        }
    }

    @Override
    public void setEnemyTarget(AbstractWiz enemyTarget) {
        body.getVelocity().setZero();
        setCurrentAction(WizAction.WEAPON_ATTACK);
        super.setEnemyTarget(enemyTarget);
        getBaseAttack().setIsActive(true);
    }

    public Circle getTargetLocation() {
        return targetLocation;
    }
}