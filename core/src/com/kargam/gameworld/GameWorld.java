package com.kargam.gameworld;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.kargam.gameobjects.Arena;
import com.kargam.gameobjects.Enemy;
import com.kargam.gameobjects.Player;
import com.kargam.helpers.AssetLoader;
import com.kargam.helpers.CollisionDetector;
import com.kargam.screens.GameScreen;
import com.kargam.wizs.Constants;

/**
 * Initialises and contains wizs and arena
 *
 * @author Karl
 */
public class GameWorld extends Stage {

    GameScreen gameScreen;

    //Wizs
    private Player player;
    private Array<Enemy> enemies;

    // Game arena
    private Arena arena;



    public GameWorld(OrthographicCamera gameCam, GameScreen gameScreen) {


        super(new ExtendViewport(Constants.ARENA_WIDTH / 10, Constants.ARENA_HEIGHT, gameCam));

        this.gameScreen = gameScreen;

        CollisionDetector.init(this);

        arena = new Arena();

        player = new Player("Player", new Vector2(Constants.WIZ_WIDTH * 4, Constants.ARENA_HEIGHT / 2),
                this, AssetLoader.ssBland);

        enemies = new Array<Enemy>();


        addActor(player);
        player.init();
        setDebugAll(true);
    }

    @Override
    public void act(float delta) {

        if (enemies.size == 0) {
            newRound();
        } else if (player.getCurrentHealth() <= 0) {
            restart();
        } else {
            super.act(delta);
        }

        for (int i = 0; i < enemies.size; i++) {
            Enemy e = enemies.get(i);
            if (e.getCurrentHealth() <= 0) {
                enemies.removeValue(e, false);
            }
        }
    }

    public void restart() {
        player.reset();
        newRound();
    }

    /**
     * Resets enemies back to starting positions.
     */
    private void newRound() {
        int n = 1;

        for (int i = 0; i < enemies.size; i++) {
            enemies.get(i).destroy();
        }
        enemies.clear();

        //Bottom left
        enemies.add(new Enemy("Enemy" + n++, new Vector2(Constants.ARENA_WIDTH - Constants.WIZ_WIDTH * 4, Constants.WIZ_HEIGHT + 64), this, AssetLoader.ssSekeleton));

        //Top left
        enemies.add(new Enemy("Enemy" + n++, new Vector2(Constants.ARENA_WIDTH - Constants.WIZ_WIDTH * 4, Constants.ARENA_HEIGHT - Constants.WIZ_HEIGHT), this, AssetLoader.ssSekeleton));

        //Top right
        enemies.add(new Enemy("Enemy" + n++, new Vector2(Constants.ARENA_WIDTH - Constants.WIZ_WIDTH, Constants.ARENA_HEIGHT - Constants.WIZ_HEIGHT), this, AssetLoader.ssSekeleton));

        //Bottom right
        enemies.add(new Enemy("Enemy" + n++, new Vector2(Constants.ARENA_WIDTH - Constants.WIZ_WIDTH, Constants.WIZ_HEIGHT + 64), this, AssetLoader.ssSekeleton));

        for (Enemy e : enemies) {
            addActor(e);
            e.init();
        }
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Array<Enemy> getEnemies() {
        return enemies;
    }

    public void setEnemies(Array<Enemy> enemies) {
        this.enemies = enemies;
    }


    public Arena getArena() {
        return arena;
    }

    public void setArena(Arena arena) {
        this.arena = arena;
    }

    public GameScreen getGameScreen() {
        return gameScreen;
    }

    public void setGameScreen(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }
}
