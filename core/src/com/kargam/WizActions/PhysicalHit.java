package com.kargam.WizActions;

import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.WizAction;
import com.kargam.gameworld.GameWorld;
import com.kargam.wizs.Constants;

/**
 * Created by Karl on 14/11/2014.
 */
public class PhysicalHit extends AbstractWizAction {

    int knockBackStrength = 10;

    public PhysicalHit(AbstractWiz owner, GameWorld world) {
        super(owner, world);
    }

    @Override
    protected void setBaseCooldown() {
        setCooldownTime(Constants.BASE_PHYSICAL_HIT_COOLDOWN);
    }

    @Override
    protected void setDamage() {
        setDamage(1);
    }

    @Override
    public void use(float posX, float posY, AbstractWiz target) {

        owner.setCurrentAction(WizAction.WEAPON_ATTACK);

        if (getCurrentCooldown() <= 0 && owner.isMoveable()) {

            owner.setImmobileTimer(0.5f);
            target.reduceHealth(damage);
            setCurrentCooldown(getCooldownTime());

            /*
            if (target.isMoveable()) {
                Vector2 hitDirection = new Vector2();
                target.getBody().getHitBox().getPosition(hitDirection);
                hitDirection.sub(owner.getBody().getHitBox().getPosition(new Vector2()));
                hitDirection = hitDirection.nor();
                hitDirection.x *= knockBackStrength;
                hitDirection.y *= knockBackStrength;
                target.knockBack(hitDirection);
            }
            */
        }
    }
}
