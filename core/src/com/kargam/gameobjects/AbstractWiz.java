package com.kargam.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.kargam.WizActions.AbstractWizAction;
import com.kargam.gameworld.GameWorld;
import com.kargam.renderers.WizRenderer;
import com.kargam.wizs.Constants;

/**
 * Superclass for all characters.
 *
 * @author Karl
 */
public abstract class AbstractWiz extends AbstractWizObject {
    protected int maxVelocity;
    protected WizBody body;
    protected boolean isFriendly;
    private String name;
    private GameWorld world;
    private float maxHealth;
    private float currentHealth;
    private Array<Projectile> projectiles;
    private int projectileDamage;
    private int maxProjectileVelocity;
    private float baseRateOfFire = Constants.BASE_RATE_OF_FILE;
    private float fireCountdown = 0;

    private Array<AbstractWizAction> wizActions;

    private boolean isMoveable = true;
    private float immobileTimer = 0;

    private int radius;

    private WizRenderer wizRenderer;
    private Circle targetLocation;

    private AbstractWiz enemyTarget;

    private AbstractWizAction baseAttack;


    //Current action
    private WizAction currentAction;


    public AbstractWiz(String name, Vector2 position, GameWorld world, boolean isFriendly, Texture texture) {
        this.setWorld(world);
        this.isFriendly = isFriendly;
        currentAction = WizAction.NOTHING;
        this.name = name;

        maxHealth = 10;
        currentHealth = 10;

        projectiles = new Array<Projectile>();
        setProjectileDamage(1);

        maxVelocity = Constants.BASE_WIZ_MAX_VELOCITY;

        body = new WizBody(this, position);


        maxProjectileVelocity = 100;


        wizActions = new Array<AbstractWizAction>();


        wizRenderer = new WizRenderer(this, texture);
    }

    public void init() {
        Vector2 initPos = new Vector2();
        getStage().screenToStageCoordinates(body.getHitBox().getPosition(initPos));
        setPosition(initPos.x, initPos.y);
    }

    @Override
    public void act(float delta) {

        destroyProjectiles();

        for (int i = 0; i < projectiles.size; i++) {
            projectiles.get(i).update(delta);
        }

        // handles immobility
        if (!isMoveable && immobileTimer > 0) {
            setImmobileTimer(immobileTimer - delta);
            if (immobileTimer <= 0) {
                isMoveable = true;
                body.getVelocity().setZero();
            }
        }
        wizRenderer.updateBlinker(currentHealth / maxHealth);

        for (int i = 0; i < getWizActions().size; i++) {
            getWizActions().get(i).reduceCooldown(delta);
        }

        baseAttack.reduceCooldown(delta);
    }

    public abstract void reduceHealth(int damage);

    public void destroy() {
        getProjectiles().clear();
        remove();
    }

    protected void destroyProjectiles() {
        for (int i = 0; i < projectiles.size; i++) {
            Projectile p = projectiles.get(i);
            if (!p.isVisible()) {
                projectiles.removeIndex(i);
            }
        }

    }

    public void knockBack(Vector2 direction) {

        this.setVelocity(direction);
        this.setMoveable(false);
        this.setImmobileTimer(0.5f);

        for (int i = 0; i < wizActions.size; i++) {
            wizActions.get(i).setIsActive(false);
        }
    }

    public float getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealth(float currentHealth) {
        this.currentHealth = currentHealth;
        if (currentHealth > maxHealth) {
            currentHealth = maxHealth;
        }
    }

    public void setVelocity(Vector2 velocity) {
        velocity.nor();
        velocity.set(velocity.x * getMaxVelocity(), velocity.y
                * getMaxVelocity());
        body.setVelocity(velocity);
    }

    @Override
    public int getScreenY() {
        return (int) body.getCenter().y;
    }

    @Override
    public boolean equals(Object o){
        return ((AbstractWiz) o).name.equalsIgnoreCase(name);
    }

    @Override
    public void render(float runTime, float delta, ShapeRenderer renderer, SpriteBatch batch) {
        wizRenderer.render(runTime, delta, renderer, batch);
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setMaxHealth(float maxHealth) {
        this.maxHealth = maxHealth;
    }

    public Array<Projectile> getProjectiles() {
        return projectiles;
    }

    public int getProjectileDamage() {
        return projectileDamage;
    }

    public void setProjectileDamage(int projectileDamage) {
        this.projectileDamage = projectileDamage;
    }

    public boolean isFriendly() {
        return isFriendly;
    }

    public void setFriendly(boolean isFriendly) {
        this.isFriendly = isFriendly;
    }

    public float getMaxVelocity() {
        return maxVelocity;
    }

    public void setMaxVelocity(int maxVelocity) {
        this.maxVelocity = maxVelocity;
    }

    public float getMaxProjectileVelocity() {
        return maxProjectileVelocity;
    }

    public void setMaxProjectileVelocity(int maxProjectileVelocity) {
        this.maxProjectileVelocity = maxProjectileVelocity;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public WizRenderer getWizRenderer() {
        return wizRenderer;
    }

    public void setWizRenderer(WizRenderer wizRenderer) {
        this.wizRenderer = wizRenderer;
    }

    public GameWorld getWorld() {
        return world;
    }

    public void setWorld(GameWorld world) {
        this.world = world;
    }

    public float getImmobileTimer() {
        return immobileTimer;
    }

    public void setImmobileTimer(float immobileTimer) {
        this.immobileTimer = immobileTimer;
    }

    public boolean isMoveable() {
        return isMoveable;
    }

    public void setMoveable(boolean isMoveable) {
        this.isMoveable = isMoveable;
    }

    public Array<AbstractWizAction> getWizActions() {
        return wizActions;
    }

    public float getBaseRateOfFire() {
        return baseRateOfFire;
    }

    public void setBaseRateOfFire(float baseRateOfFire) {
        this.baseRateOfFire = baseRateOfFire;
    }

    public float getFireCountdown() {
        return fireCountdown;
    }

    public void setFireCountdown(float fireCountdown) {
        this.fireCountdown = fireCountdown;
    }

    public WizBody getBody() {
        return body;
    }

    public void setBody(WizBody body) {
        this.body = body;
    }

    public AbstractWiz getEnemyTarget() {
        return enemyTarget;
    }

    public void setEnemyTarget(AbstractWiz enemyTarget) {
        this.enemyTarget = enemyTarget;
    }

    public WizAction getCurrentAction() {
        return currentAction;
    }

    public void setCurrentAction(WizAction currentAction) {
        this.currentAction = currentAction;
    }

    public AbstractWizAction getBaseAttack() {
        return baseAttack;
    }

    public void setBaseAttack(AbstractWizAction baseAttack) {
        this.baseAttack = baseAttack;
    }
}
