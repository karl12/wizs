package com.kargam.gameobjects;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.kargam.WizActions.PhysicalHit;
import com.kargam.gameworld.GameWorld;

/**
 * Hostile wiz.
 *
 * @author Karl
 */

public class Enemy extends AbstractWiz {

    private Player player;

    public Enemy(String name, Vector2 position, GameWorld world, Texture texture) {
        super(name, position, world, false, texture);
        player = world.getPlayer();
        setEnemyTarget(player);

        setBaseAttack(new PhysicalHit(this, world));
    }

    public void act(float delta) {
        super.act(delta);

        moveToPlayer();
        if (getCurrentHealth() <= 0) {
            destroy();
        }

        if (getWorld().getPlayer().getBody().getCenter().dst(this.getBody().getCenter()) < this.getBody().getHitBox().getWidth() * 1.05) {
            getBaseAttack().use(0, 0, player);
        } else {
            moveToPlayer();
            body.update(delta);
        }
    }

    private void moveToPlayer() {
        setCurrentAction(WizAction.MOVING);
        Vector2 playerPos = new Vector2();
        player.getBody().getHitBox().getPosition(playerPos);

        playerPos.sub(getBody().getHitBox().getPosition(new Vector2()));

        this.setVelocity(playerPos);
    }
    @Override
    public void reduceHealth(int damage) {
        setCurrentHealth(getCurrentHealth() - damage);
    }

}