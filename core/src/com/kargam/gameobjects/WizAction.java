package com.kargam.gameobjects;

/**
 * Created by Karl on 14/11/2014.
 */
public enum WizAction {
    NOTHING, MOVING, WEAPON_ATTACK, CASTING
}
