package com.kargam.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.kargam.WizActions.AbstractWizAction;
import com.kargam.gameobjects.Enemy;
import com.kargam.gameobjects.Player;
import com.kargam.gameworld.GameWorld;
import com.kargam.wizsui.RightControlSet;

public class InputHandler implements InputProcessor {

    private GameWorld world;
    private int movementPointer = -1;

    public InputHandler(GameWorld world,
                        RightControlSet rightControlSet) {
        this.world = world;
    }

    public void pollInput() {
        if (Gdx.input.isTouched()) {
            heldOrDragged(Gdx.input.getX(), Gdx.input.getY(), 0);
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {


        Vector2 localPos = world.getPlayer().screenToLocalCoordinates(new Vector2(screenX, screenY));
        Vector2 stagePos = world.getPlayer().localToStageCoordinates(localPos);

        Player p = world.getPlayer();

        for (int i = 0; i < p.getWizActions().size; i++) {
            AbstractWizAction s = p.getWizActions().get(i);
            if (s.isActive()) {
                s.use(stagePos.x, stagePos.y, null);
                movementPointer = -1;
                return true;
            }
        }

        //if enemy clicked then attack
        for (int i = 0; i < world.getEnemies().size; i++) {
            Enemy e = world.getEnemies().get(i);
            if (e.getBody().getHitBox().contains(stagePos)) {
                p.setEnemyTarget(e);
                movementPointer = -1;
                return true;
            }
        }

        movementPointer = pointer;
        world.getPlayer().setTargetLocation(stagePos.x, stagePos.y);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if(pointer == movementPointer){
            movementPointer = -1;
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {

        return heldOrDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        // TODO Auto-generated method stub
        return false;
    }

    private boolean heldOrDragged(int screenX, int screenY, int pointer) {

        if (movementPointer == pointer) {
            Vector2 localPos = world.getPlayer().screenToLocalCoordinates(new Vector2(screenX, screenY));
            Vector2 stagePos = world.getPlayer().localToStageCoordinates(localPos);
            world.getPlayer().setTargetLocation(stagePos.x, stagePos.y);
            return true;
        }
        return false;
    }
}
