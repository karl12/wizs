package com.kargam.helpers;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Takes a generated LPC sprite sheet character and separates it in to animations.
 *
 * Created by Karl on 01/11/2014.
 */
public class LPCAnimationSet {

    private Texture t;

    //Casts
    private Animation castUp;
    private Animation castLeft;
    private Animation castDown;
    private Animation castRight;

    //Thrusts
    private Animation thrustUp;
    private Animation thrustLeft;
    private Animation thrustDown;
    private Animation thrustRight;

    //Walks
    private Animation walkUp;
    private Animation walkLeft;
    private Animation walkDown;
    private Animation walkRight;


    //Weapons
    private Animation weaponUp;
    private Animation weaponLeft;
    private Animation weaponDown;
    private Animation weaponRight;

    //Shoot
    private Animation shootUp;
    private Animation shootLeft;
    private Animation shootDown;
    private Animation shootRight;

    //Die
    private Animation die;

    //Counts y position for main animations
    private int heightCounter;
    //Sprite height
    private int spriteHeight = 64;

    public LPCAnimationSet(Texture t) {
        this.t = t;
        generateMainAnimations();
    }

    private void generateMainAnimations() {
        heightCounter = 0;

        //Generate casts
        castUp = generateAnimations(7);
        castLeft = generateAnimations(7);
        castDown = generateAnimations(7);
        castRight = generateAnimations(7);

        //Generate Thrusts
        thrustUp = generateAnimations(8);
        thrustLeft = generateAnimations(8);
        thrustDown = generateAnimations(8);
        thrustRight = generateAnimations(8);

        //Generate walks
        walkUp = generateAnimations(9);
        walkLeft = generateAnimations(9);
        walkDown = generateAnimations(9);
        walkRight = generateAnimations(9);

        //Generate weapons
        weaponUp = generateAnimations(6);
        weaponLeft = generateAnimations(6);
        weaponDown = generateAnimations(6);
        weaponRight = generateAnimations(6);

        //Generate shoots
        shootUp = generateAnimations(10);
        shootLeft = generateAnimations(10);
        shootDown = generateAnimations(10);
        shootRight = generateAnimations(10);

        die = generateAnimations(6);
    }

    private Animation generateAnimations(int num) {
        Array<TextureRegion> array = new Array<TextureRegion>();
        int x = 0;
        for (int i = 0; i < num; i++) {
            TextureRegion tr = new TextureRegion(t, x, heightCounter, 64, spriteHeight);
            array.add(tr);
            x += 64;
        }
        Animation a = new Animation(0.03f, array, Animation.PlayMode.LOOP);
        heightCounter += spriteHeight;
        return a;
    }

    public Animation getCastUp() {
        return castUp;
    }

    public Animation getCastLeft() {
        return castLeft;
    }

    public Animation getCastDown() {
        return castDown;
    }

    public Animation getCastRight() {
        return castRight;
    }

    public Animation getThrustUp() {
        return thrustUp;
    }

    public Animation getThrustLeft() {
        return thrustLeft;
    }

    public Animation getThrustDown() {
        return thrustDown;
    }

    public Animation getThrustRight() {
        return thrustRight;
    }

    public Animation getWalkUp() {
        return walkUp;
    }

    public Animation getWalkLeft() {
        return walkLeft;
    }

    public Animation getWalkDown() {
        return walkDown;
    }

    public Animation getWalkRight() {
        return walkRight;
    }

    public Animation getWeaponUp() {
        return weaponUp;
    }

    public Animation getWeaponLeft() {
        return weaponLeft;
    }

    public Animation getWeaponDown() {
        return weaponDown;
    }

    public Animation getWeaponRight() {
        return weaponRight;
    }

    public Animation getShootUp() {
        return shootUp;
    }

    public Animation getShootLeft() {
        return shootLeft;
    }

    public Animation getShootDown() {
        return shootDown;
    }

    public Animation getShootRight() {
        return shootRight;
    }

    public Animation getDie() {
        return die;
    }
}
