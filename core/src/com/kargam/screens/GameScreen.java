package com.kargam.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.InputHandler;
import com.kargam.renderers.GameRenderer;
import com.kargam.wizs.Constants;
import com.kargam.wizs.WizsGame;
import com.kargam.wizsui.UserInterface;

public class GameScreen implements Screen {
    //UI
    UserInterface ui;
    private GameRenderer renderer;
    private GameWorld world;
    private WizsGame wizsGame;
    private float runTime = 0;

    private OrthographicCamera gameCam;
    private OrthographicCamera uiCam;

    private boolean isPaused = false;

    private InputMultiplexer inputMulti;
    private Stage uiStage;
    private InputHandler handler;

    public GameScreen(WizsGame wizsGame) {

        Constants.WIZ_SPRITE_RENDER_SIZE = 64;
        Constants.WIZ_WIDTH = (int) Math.round(Constants.WIZ_SPRITE_RENDER_SIZE * .5);
        Constants.WIZ_HEIGHT = (int) Math.round(Constants.WIZ_SPRITE_RENDER_SIZE * .8);

        Constants.BASE_WIZ_MAX_VELOCITY = Constants.ARENA_HEIGHT / 5;

        uiStage = new Stage();

        this.setWizsGame(wizsGame);

        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();

        gameCam = new OrthographicCamera();
        gameCam.setToOrtho(false, 30 * (h / w), 20);
        gameCam.update();

        uiCam = new OrthographicCamera();
        uiCam.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiCam.update();


        world = new GameWorld(gameCam, this);
        ui = new UserInterface(world, uiStage, wizsGame);

        renderer = new GameRenderer(world, gameCam, uiCam, ui);

        inputMulti = new InputMultiplexer();
        handler = new InputHandler(world,
                ui.getRightControlSet());

        inputMulti.addProcessor(uiStage);
        inputMulti.addProcessor(handler);

        Gdx.input.setInputProcessor(inputMulti);
    }

    @Override
    public void render(float delta) {
        if (!isPaused) {
            runTime += delta;

            world.act(delta);
            renderer.render(runTime, delta);


            gameCam.position.x = world.getPlayer().getBody().getCenter().x;
            //gameCam.position.y = world.getPlayer().getBody().getCenter().y;
            gameCam.update();

            renderer.updateCams();

            handler.pollInput();
        }
    }

    @Override
    public void resize(int width, int height) {
        // TODO Auto-generated method stub
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(inputMulti);
        isPaused = false;
    }

    @Override
    public void hide() {
        isPaused = true;
        Gdx.input.setInputProcessor(null);

    }

    @Override
    public void pause() {
        isPaused = true;

    }

    @Override
    public void resume() {
        isPaused = false;

    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    public WizsGame getWizsGame() {
        return wizsGame;
    }

    public void setWizsGame(WizsGame wizsGame) {
        this.wizsGame = wizsGame;
    }

    public InputMultiplexer getInputMulti() {
        return inputMulti;
    }

    public void setInputMulti(InputMultiplexer inputMulti) {
        this.inputMulti = inputMulti;
    }

    public Stage getUiStage() {
        return uiStage;
    }

    public void setUiStage(Stage uiStage) {
        this.uiStage = uiStage;
    }

    public GameWorld getGameWorld(){
        return world;
    }

}
