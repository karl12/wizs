package com.kargam.renderers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.Player;
import com.kargam.gameobjects.WizAction;
import com.kargam.helpers.LPCAnimationSet;
import com.kargam.wizs.Constants;

/**
 * Renders the abstractWiz in the game world.
 *
 * @author Karl
 */
public class WizRenderer {

    private Blinker blinker;
    private AbstractWiz abstractWiz;
    private Player player;
    private LPCAnimationSet animations;
    private Animation currentAnimation;
    private Direction direction;
    private WizAction currentAction;

    public WizRenderer(AbstractWiz abstractWiz, Texture spriteSheet) {
        animations = new LPCAnimationSet(spriteSheet);

        blinker = new Blinker();
        this.abstractWiz = abstractWiz;

        if (abstractWiz.isFriendly()) {
            this.player = (Player) abstractWiz;
        }

        direction = Direction.DOWN;

    }

    public void render(float runTime, float delta, ShapeRenderer renderer, SpriteBatch batch) {

        Vector2 pos = abstractWiz.getBody().getHitBox().getPosition(new Vector2());
        pos.sub(abstractWiz.getBody().getHitBox().getWidth() / 2, 0);


        updateAnimation();

        if (!blinker.shouldBlink(delta)) {
            batch.begin();
            if (abstractWiz.getCurrentAction().equals(WizAction.NOTHING)) {
                batch.draw(currentAnimation.getKeyFrame(0), pos.x, pos.y, Constants.WIZ_SPRITE_RENDER_SIZE, Constants.WIZ_SPRITE_RENDER_SIZE);
            } else {
                batch.draw(currentAnimation.getKeyFrame(runTime), pos.x, pos.y, Constants.WIZ_SPRITE_RENDER_SIZE, Constants.WIZ_SPRITE_RENDER_SIZE);
            }
            batch.end();
        }

        //debugging only
        if (Constants.DEBUGGING_MODE) {
            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.BLUE);
            renderer.rect(abstractWiz.getBody().getHitBox().x, abstractWiz.getBody().getHitBox().y,
                    abstractWiz.getBody().getHitBox().getWidth(), abstractWiz.getBody().getHitBox().getHeight());
            renderer.end();

            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.RED);
            renderer.rect(abstractWiz.getBody().getGroundBox().x, abstractWiz.getBody().getGroundBox().y,
                    abstractWiz.getBody().getGroundBox().getWidth(), abstractWiz.getBody().getGroundBox().getHeight());
            renderer.end();

            if (player != null) {
                renderer.begin(ShapeRenderer.ShapeType.Line);
                renderer.setColor(Color.GREEN);
                renderer.circle(player.getTargetLocation().x, player.getTargetLocation().y, 20);
                renderer.end();
            }
        }
    }

    public void updateBlinker(float healthPercent) {
        blinker.setBlinkSpeed(healthPercent);
    }

    private void updateAnimation() {

        updateDirection();
        currentAction = abstractWiz.getCurrentAction();

        switch (direction) {
            case UP:
                if (currentAction.equals(WizAction.WEAPON_ATTACK)) {
                    currentAnimation = animations.getWeaponUp();

                } else if (currentAction.equals(WizAction.CASTING)) {
                    currentAnimation = animations.getCastUp();
                } else {
                    currentAnimation = animations.getWalkUp();
                }
                break;
            case DOWN:
                if (currentAction.equals(WizAction.WEAPON_ATTACK)) {
                    currentAnimation = animations.getWeaponDown();

                } else if (currentAction.equals(WizAction.CASTING)) {
                    currentAnimation = animations.getCastDown();
                } else {
                    currentAnimation = animations.getWalkDown();
                }
                break;
            case RIGHT:
                if (currentAction.equals(WizAction.WEAPON_ATTACK)) {
                    currentAnimation = animations.getWeaponRight();
                } else if (currentAction.equals(WizAction.CASTING)) {
                    currentAnimation = animations.getCastRight();
                } else {
                    currentAnimation = animations.getWalkRight();
                }
                break;
            case LEFT:
                if (currentAction.equals(WizAction.WEAPON_ATTACK)) {
                    currentAnimation = animations.getWeaponLeft();
                } else if (currentAction.equals(WizAction.CASTING)) {
                    currentAnimation = animations.getCastLeft();
                } else {
                    currentAnimation = animations.getWalkLeft();
                }
        }

        if (abstractWiz.getCurrentAction().equals(WizAction.CASTING)) {
            currentAnimation.setPlayMode(Animation.PlayMode.NORMAL);
            currentAnimation.setFrameDuration(0.1f);
        } else {

            currentAnimation.setPlayMode(Animation.PlayMode.LOOP);
            currentAnimation.setFrameDuration(0.03f);
        }

    }

    private void updateDirection() {
        float dir = -1;

        if (abstractWiz.getBody().getVelocity().len() > 1) {
            dir = abstractWiz.getBody().getVelocity().angle();
        } else if (abstractWiz.getCurrentAction().equals(WizAction.WEAPON_ATTACK)) {
            Vector2 targetDirection = new Vector2(abstractWiz.getEnemyTarget().getBody().getCenter());
            Vector2 currentLocation = new Vector2(abstractWiz.getBody().getCenter());
            dir = targetDirection.sub(currentLocation).angle();
        }

        if (dir >= 315 && dir < 360 ||
                dir >= 0 && dir < 45) {
            direction = Direction.RIGHT;
        } else if (dir >= 45 && dir < 135) {
            direction = Direction.UP;
        } else if (dir >= 135 && dir < 225) {
            direction = Direction.LEFT;
        } else if (dir >= 225 && dir < 315) {
            direction = Direction.DOWN;
        }
    }

    private enum Direction {
        UP, RIGHT, LEFT, DOWN
    }
}
