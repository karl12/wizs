package com.kargam.renderers;

/**
 * Helper class for blinking the wiz based on current health. Based on class
 * found on stackoverflow by Alon Zilberman.
 *
 * @author Karl
 */

public class Blinker {
    private int BLINKING_FRAMES = 30;

    private boolean isBlinking;
    private int blinkFrameCounter;

    public Blinker() {
        this.blinkFrameCounter = 0;
        this.isBlinking = false;
    }

    public boolean shouldBlink(float delta) {
        if (isBlinking) {
            blinkFrameCounter++;
            if (blinkFrameCounter % BLINKING_FRAMES == 0) {
                return true;
            }
        } else {
            isBlinking = false;
        }
        return false;
    }

    public boolean isBlinking() {
        return isBlinking;
    }

    public void setBlinking(boolean isBlinking) {
        this.isBlinking = isBlinking;
    }

    public void setBlinkSpeed(float healthPercent) {
        if (healthPercent <= 0.9) {
            isBlinking = true;
            BLINKING_FRAMES = Math.round(healthPercent * 40);
        }

    }
}
