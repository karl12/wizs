package com.kargam.gameobjects;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.CollisionDetector;
import com.kargam.wizs.Constants;


/**
 * Projectiles fired by the Wizs. In the future this class will be abstract.
 *
 * @author Karl
 */
public class Projectile extends AbstractWizObject {
    private GameWorld world;

    private int damage;

    private boolean isVisible;
    private boolean isFriendly;

    private int radius = 4;

    private Circle boundingCircle;

    private Vector2 velocity;

    private Animation animation;

    public Projectile(Vector2 position, Vector2 velocity, int damage,
                      boolean isFriendly, GameWorld world, Animation animation) {

        this.world = world;
        this.animation = animation;
        this.velocity = velocity;
        this.isFriendly = isFriendly;

        boundingCircle = new Circle(position.x, position.y, Constants.PROJECTILE_RADIUS);

        this.damage = damage;

        isVisible = true;
    }

    public void update(float delta) {
        Vector2 newPos = new Vector2(boundingCircle.x, boundingCircle.y);
        newPos.add(velocity.cpy().scl(delta));
        boundingCircle.setPosition(newPos);

        CollisionDetector.hasProjectileCollided(this);
    }

    public void hitWiz(AbstractWiz abstractWiz) {
        isVisible = false;
        abstractWiz.reduceHealth(damage);
    }

    public void hitWall() {
        isVisible = false;
    }

    @Override
    public int getScreenY() {
        return (int) boundingCircle.y;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }


    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public Circle getBoundingCircle() {
        return boundingCircle;
    }

    public void setBoundingCircle(Circle boundingCircle) {
        this.boundingCircle = boundingCircle;
    }

    public boolean isFriendly() {
        return isFriendly;
    }

    public void setFriendly(boolean isFriendly) {
        this.isFriendly = isFriendly;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    @Override
    public float getY() {
        return (int) boundingCircle.y;
    }

    @Override
    public void render(float runTime, float delta, ShapeRenderer renderer, SpriteBatch batch) {

        batch.begin();
        batch.draw(animation.getKeyFrame(runTime), boundingCircle.x-boundingCircle.radius, boundingCircle.y-boundingCircle.radius);
        batch.end();

        if(Constants.DEBUGGING_MODE){
            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.YELLOW);
            renderer.circle(boundingCircle.x, boundingCircle.y, boundingCircle.radius);
            renderer.end();
        }
    }
}
