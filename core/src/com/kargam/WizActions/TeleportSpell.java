package com.kargam.WizActions;

import com.badlogic.gdx.math.Vector2;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.Player;
import com.kargam.gameobjects.WizAction;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.AssetLoader;
import com.kargam.wizs.Constants;

public class TeleportSpell extends AbstractWizAction {

    public TeleportSpell(Player player, GameWorld world) {
        super(player, world);
        actionIcon = AssetLoader.teleportTxr;
    }


    @Override
    protected void setBaseCooldown() {
        setCooldownTime(Constants.BASE_TELEPORT_COOLDOWN);
    }

    @Override
    protected void setDamage() {
        setDamage(0);
    }

    @Override
    public void use(float posX, float posY, AbstractWiz target) {
        owner.getBody().setPosition(new Vector2(posX, posY));
        setCurrentCooldown(getCooldownTime());
        isActive = false;
        owner.getBody().setVelocity(new Vector2());
        owner.setCurrentAction(WizAction.NOTHING);
    }

    @Override
    public void toggleActive() {
        super.toggleActive();
        if (isActive()) {
            owner.setCurrentAction(WizAction.CASTING);
            owner.setVelocity(new Vector2());
        }
    }
}
