package com.kargam.WizActions;

import com.badlogic.gdx.math.Vector2;
import com.kargam.gameobjects.AbstractWiz;
import com.kargam.gameobjects.Projectile;
import com.kargam.gameobjects.WizAction;
import com.kargam.gameworld.GameWorld;
import com.kargam.helpers.AssetLoader;
import com.kargam.helpers.SpellAnimationSet;
import com.kargam.wizs.Constants;

/**
 * Created by Karl on 19/11/2014.
 */
public class FireballSpell extends AbstractWizAction {

    public FireballSpell(AbstractWiz owner, GameWorld world) {
        super(owner, world);
        actionIcon = AssetLoader.fireballTxr;
        animationSet = new SpellAnimationSet(AssetLoader.fireballSpriteTxr);
    }

    @Override
    protected void setBaseCooldown() {
        setCooldownTime(Constants.BASE_FIREBALL_COOLDOWN);
    }

    @Override
    protected void setDamage() {
        setDamage(Constants.BASE_FIREBALL_DAMAGE);
    }

    @Override
    public void use(float posX, float posY, AbstractWiz target) {
        //Controls rate of fire
        if (getCurrentCooldown() <= 0){
            owner.setCurrentAction(WizAction.CASTING);

            Vector2 pos = new Vector2();
            owner.getBody().getHitBox().getPosition(pos);

            Vector2 aimPos = new Vector2(posX, posY);

            aimPos.sub(pos);

            aimPos.limit(owner.getMaxProjectileVelocity());

            owner.getProjectiles().add(
                    new Projectile(owner.getBody().getCenter().add(aimPos.cpy().scl(.3f)), aimPos, damage,
                            owner.isFriendly(), owner.getWorld(), animationSet.getAnimation(aimPos.angle())));

            setCurrentCooldown(getCooldownTime());
            isActive = false;
            owner.setCurrentAction(WizAction.NOTHING);


        }
    }
}
